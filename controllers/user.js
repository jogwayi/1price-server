const crypto = require('crypto');
const jwt = require('jsonwebtoken');


/**
 * Importing Database Models
 */
const User = require("../models/user");
const Referral = require("../models/referral");


/**
 * Helper Functions for Token
 */
function generateToken(user, access_level, callback) {

    let newToken = jwt.sign(setUserInfo(user, access_level), process.env.SESSION_SECRET, {
        expiresIn: 86400
    });

    callback(newToken);
}

function setUserInfo(request, access_level) {
    return {
        _id: request._id,
        access_level: access_level
    };
}


/**
 * Helper Functions
 */
const generateOTP = function() {
    return Math.floor(Math.random() * 90000) + 10000;
};

const sha256 = function(data) {
    return crypto.createHash("sha256").update(data).digest("base64");
};

const sendOTP = function(mobile, otp) {
    global.client.messages.create({
        to: mobile,
        from: global.from,
        body: "Verification code: " + otp,
    }, function(err, message) {
        if (err) {
            console.log(err);
        } else { console.log(message.sid); }
    });
};


/**
 * Function Exports
 */

exports.random = (req, res) => {
    let str = global.randomString(6);
    res.send('respond with a resource ' + str);
};

exports.getUsers = (req, res) => {    
    User.find({}, (err, users) => {

        if (err) {
            res.status(406).json({ success: false, message: err.message});
        } else if (!users) {
            res.status(404);
        } else {
            res.json({
                error: false,
                users: users
            });
        }
    });    
}
exports.delete = (req, res) => {
    User.findOneAndRemove({
        _id: req.params.id
    }, function(err) {
        if (err) {
            res.status(404).json({ success: false, message: 'User not found' });
        } else { 
            res.json({
                success: true,
                message: 'successful!', 
                id: req.params.id
            });
        }
    });

};
exports.update = (req, res) => {
    User.findOne({
        id: req.body.id
    }, function(err, user) {

        if (err || !user) {
            res.status(404).json({ success: false, message: 'User not found' });
        } else if (user) {
            User.update().remove();
            res.json({
                success: true,
                message: 'successful!', 
                id: req.body.id
            });
        }
    });

};
exports.postLogin = (req, res) => {

    User.findOne({
        email: req.body.email
    }, function(err, user) {

        if (err || !user) {
            res.status(404).json({ success: false, message: 'Incorrect username or password' });
        } else if (user) {
            user.comparePassword(req.body.password, (err, isMatch) => {
                if (err) {
                    res.status(401).json({ error: true, code: 401 });
                } else if (isMatch) {

                    generateToken(user, 'user', (token) => {

                        const us = {
                            "_id": user._id,
                            "email": user.email,
                            "firstname": user.firstname,
                            "othername": user.othername,
                            "ext": user.ext,
                            "mobile": user.mobile,
                            "otpVerified": user.otpVerified,
                            "mailVerified": user.mailVerified,
                            "created_at": user.created_at,
                            "updated_at": user.updated_at,
                        };

                        res.json({
                            success: true,
                            message: 'Login successful!',
                            token: token,
                            user: us
                        });

                    });

                } else {
                    res.status(401).json({ success: false, message: 'Incorrect password' });
                }
            });
        }
    });
};


exports.postProfile = (req, res) => {
    
    User.findOne({
        _id: req.body.id
    }, function(err, user) {

        if (err || !user) {
            res.status(401).json({ success: false, message: 'Not authorized' });
        } else if (user) {  
            const us = {
                "_id": user._id,
                "email": user.email,
                "firstname": user.firstname,
                "othername": user.othername,
                "ext": user.ext,
                "mobile": user.mobile,
                "otpVerified": user.otpVerified,
                "mailVerified": user.mailVerified,
                "created_at": user.created_at,
                "updated_at": user.updated_at,
            };          
            res.json(us);  
        }
    });
};

exports.verifyOTP = (req, res, next) => {
    User.findOne({
        email: req.body.email,
        otp: req.body.otp
    }, function(err, user) {
        if (err || !user) {
            res.status(404).json({ success: false, message: 'Invalid OTP' });
        } else if (user) {
            user.otpVerified = true;
            user.save();
            res.json({ success: true, message: "Phone successfully verified" });
        }
    });
};


exports.postReferral = (req, res) => {
    let code = global.randomString(8);
    let refer = new Referral({
        source_mail: req.body.source_mail,
        invite_mail: req.body.invite_mail,
        source_mobile: req.body.source_mobile,
        invited_mobile: req.body.invited_mobile,
        claimed: false,
        is_valid: true,
        created_at: new Date(),
        updated_at: new Date(),
        code: code
    });
    refer.save((err) => {
        if (err) {
            console.log(err);
            res.json({ success: false, message: err.message, code: code });
        } else {
            console.log(code);
            res.json({ success: true, message: null, code: code });
        }
    });
};


exports.postRegister = (req, res) => {

    let otp = generateOTP();

    let user = new User({
        email: req.body.email,
        firstname: req.body.firstname,
        othername: req.body.othername,
        password: req.body.password || 12345,
        ext: req.body.ext,
        mobile: req.body.mobile,
        access_level: req.body.access_level || 'user',
        otp: otp,
        otpVerified: false,
        mailVerified: false,
        created_at: new Date(),
        updated_at: new Date(),
        meta: req.body.meta,
    });

    user.save((err) => {
        if (err) {
            console.log(err);
            res.status(400).json({ success: false, message: err.message });
        } else {
            //sendOTP(req.body.ext+req.body.mobile,otp);
            console.log(otp);
            res.json({ success: true, message: null });
        }
    });

};