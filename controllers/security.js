const jwt = require('jsonwebtoken');
const validate = require('validate.js');


const contraintsForLogin = {
    email: {
        presence: true,
        email: true
    },
    password: {
        presence: true,
        length: {
            minimum: 6,
            message: 'must be 6 characters long'
        }
    }
};


const contraintsForNewAdmin = {
    firstname: {
        presence: true,
    },
    email: {
        email: true
    }
};

exports.validateLoginInput = (req, res, next) => {

    if (validate({ email: req.body.email, password: req.body.password }, contraintsForLogin) === undefined) {
        next();
    } else {
        res.status(403).json({
            error: true,
            code: 403
        });
    }

};


exports.validateNewAdminDetails = (req, res, next) => {

    if (validate(req.body, contraintsForNewAdmin) === undefined) {
        next();
    } else {
        res.status(403).json({
            error: true,
            code: 403
        });
    }

};

exports.tokenVerifyAdmin = (req, res, next) => {
    let token = '';
    if (typeof req.headers.authorization !== 'undefined') {

        let bearerHeader = req.headers.authorization;
        let bearer = bearerHeader.split(" ");
        token = bearer[1];

        jwt.verify(token, process.env.SESSION_SECRET, (err, decoded) => {

            if (err) {
                console.log(err);
                res.status(401).json({
                    error: true,
                    msg: 'Token expired login again'
                });

            } else {

                if (decoded.access_level === 'admin' || decoded.access_level === 'super admin') {
                    req.decoded = decoded;
                    return next();
                } else {
                    res.status(403).json({
                        error: true,
                        code: 403
                    });
                }
            }

        });

    } else {
        res.send(403);
    }

};


exports.superAdminVerify = (req, res, next) => {

    if (req.decoded.access_level === 'super admin') {
        console.log('super admin');
        return next();
    } else {
        console.log('not super admin');
        res.status(403).json({
            error: true,
            code: 403
        });
    }

};


exports.tokenVerify = (req, res, next) => {
    let token = '';
    if (typeof req.headers.authorization !== 'undefined') {

        let bearerHeader = req.headers.authorization;
        let bearer = bearerHeader.split(" ");
        token = bearer[1];

        jwt.verify(token, process.env.SESSION_SECRET, (err, decoded) => {

            if (err) {
                console.log(err);

                res.status(401).json({
                    error: true,
                    msg: 'Token expired login again'
                });

            } else {
                req.decoded = decoded;
                return next();
            }

        });

    } else {
        res.send(403);
    }

};