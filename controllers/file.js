var http = require('http');
var formidable = require('formidable');
var fs = require('fs');
const path = require('path');


/**
 * Importing Database Models
 */
const BinaryFile = require("../models/file");
exports.addFile = (req, res) => {
    var form = new formidable.IncomingForm();
    form.multiples = false
    form.keepExtensions = true;
    form.parse(req, function (err, fields, files) {
        var oldpath = files.filetoupload.path;
        var relativePath =  global.uploadsPath+"/" + files.filetoupload.name;
        console.log(oldpath,relativePath,files.filetoupload.extension);
        let f = oldpath.split(".");
        let ext = f[f.length-1];
        let str = global.randomString(10);
        let file = new BinaryFile({
            name: files.filetoupload.name,
            slag: str
        });
        file.save(function(ferr,f) {
            var newpath = global.uploadsPath+"/" + f.id+"."+ext;
            fs.rename(oldpath, newpath, function (err) {
            if (err) throw err;            
                if(!ferr){
                    res.json({
                        success: true,
                        message: 'Saved!',
                        id: f.id
                    });
                }else{
                    res.json({ success: false, message: err.message});
                }
            });
        });
    });
};