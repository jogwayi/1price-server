const TempProduct = require('../models/temp-product');
const Product = require('../models/product');

exports.addTempProduct = (req, res) => {

    const product = new TempProduct({
        Category:req.body.Category,
        Url:req.body.Url,
        Brand:req.body.Brand,
        productname:req.body.productname,
        shopurls:req.body.shopurls,
        Specifications:req.body.Specifications,
        images:req.body.images
    });
    product.save((err,o) => {
        if(!err){
            res.status(200).json({
                success: true,
                message: 'Saved!',
                id: o.id
            });
        }else{
            res.status(406).json({ success: false, message: err.message});
        }
    });
};
exports.getProducts = (req, res) => {
    Product.find({}, (err, products) => {
        
        if (err) {
            res.status(406).json({ success: false, message: err.message});
        } else if (!products) {
            res.status(404);
        } else {
            res.json({
                error: false,
                products: products
            });
        }
    }); 
};
exports.getTempProducts = (req, res) => {
    TempProduct.find({}, (err, products) => {        
        if (err) {
            res.status(406).json({ success: false, message: err.message});
        } else if (!products) {
            res.status(404);
        } else {
            res.json({
                error: false,
                products: products
            });
        }
    }); 
};
exports.approveProduct = (req, res) => {
    
    const product = new Product({
        Category:req.body.Category,
        Url:req.body.Url,
        productname:req.body.productname,
        shopurls:req.body.shopurls,
        Specifications:req.body.Specifications
    });
    product.save((err,o) => {
        
        if(!err){
            res.status(200).json({
                success: true,
                message: 'Saved!',
                id: o.id
            });
        }else{
            res.status(406).json({ success: false, message: err.message});
        }
    });
    
}        

exports.addProduct = (req, res) => {
    
    const product = new Product({
        Category:req.body.Category,
        Url:req.body.Url,
        productname:req.body.productname,
        shopurls:req.body.shopurls,
        Specifications:req.body.Specifications
    });
    product.save((err,o) => {
        
        if(!err){
            res.status(200).json({
                success: true,
                message: 'Saved!',
                id: o.id
            });
        }else{
            res.status(406).json({ success: false, message: err.message});
        }
    });
    
}
exports.updateProduct = (req, res) => {
    
        const product = new Product({
            Category:req.body.Category,
            Url:req.body.Url,
            productname:req.body.productname,
            shopurls:req.body.shopurls,
            Specifications:req.body.Specifications
        });
        product.save((err,o) => {
            
            if(!err){
                res.status(200).json({
                    success: true,
                    message: 'Saved!',
                    id: o.id
                });
            }else{
                res.status(406).json({ success: false, message: err.message});
            }
        });
        
}
       
exports.deleteProduct = (req, res) => {
    
    const product = new Product({
        Category:req.body.Category,
        Url:req.body.Url,
        productname:req.body.productname,
        shopurls:req.body.shopurls,
        Specifications:req.body.Specifications
    });
    product.save((err,o) => {
        
        if(!err){
            res.status(200).json({
                success: true,
                message: 'Saved!',
                id: o.id
            });
        }else{
            res.status(406).json({ success: false, message: err.message});
        }
    });
    
}        