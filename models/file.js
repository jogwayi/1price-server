const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Schema defines how the user's data will be stored in MongoDB
const FileSchema = new mongoose.Schema({
    name:String,
    slag:String
}, { timestamps: true });
const File = global.mongoose.model('BinaryFile', FileSchema);

module.exports = File;