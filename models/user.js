const Schema = global.mongoose.Schema;
const mongooseHidden = require('mongoose-hidden')({ defaultHidden: {} })
const bcrypt = require('bcrypt');

// create a schema
const userSchema = new Schema({
    addedBy: Schema.Types.ObjectId,
    actualRestaurantId: Schema.Types.ObjectId,
    emailVerified: { type: Boolean, default: false },
    firstname: String,
    othername: String,
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true, hide: true },
    access_level: { type: String },
    location: String,
    otpVerified: { type: Boolean, hide: true },
    mailVerified: { type: Boolean, hide: true },
    otp: { type: Number, hide: true },
    mobile: { type: Number, required: true, unique: true },
    ext: { type: String },
    meta: {
        age: Number,
        website: String
    }
}, { timestamps: true });


/**
 * Password hash middleware.
 */
userSchema.pre('save', function save(next) {
    const user = this;
    if (!user.isModified('password')) { return next(); }
    bcrypt.genSalt(10, (err, salt) => {
        if (err) { return next(err); }
        bcrypt.hash(user.password, salt, (err, hash) => {
            if (err) { return next(err); }
            user.password = hash;
            next();
        });
    });
});

/**
 * Helper method for validating user's password.
 */
userSchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
    bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
        cb(err, isMatch);
    });
};


userSchema.plugin(mongooseHidden);

const User = global.mongoose.model('User', userSchema);

module.exports = User;