const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require("./user");

// Schema defines how the user's data will be stored in MongoDB
const SpecificationSchema = new mongoose.Schema({
    name: String,  
    value: String,
    subspecs:[this]
}, { timestamps: true });

// Schema defines how the user's data will be stored in MongoDB
const ProductSchema = new mongoose.Schema({
    Category:String,
    Url:String,
    Brand:String,
    productname:String,
    shopurls:[{
        url: String,
        price: Number,
        dateAdded:Date,
        createdBy:{type:Schema.Types.ObjectId,ref:'User'},
        updatedBy:{type:Schema.Types.ObjectId,ref:'User'}
    }],
    images:[String],
    Specifications:[SpecificationSchema],   
}, { timestamps: true });

const Product = global.mongoose.model('Product', ProductSchema);
module.exports = Product;