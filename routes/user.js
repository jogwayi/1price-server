const express = require('express');
const router = express.Router();


/**
 * Importing Controllers
 */
const userController = require('../controllers/user');
const securityController = require('../controllers/security');


/**
 * Routes
 */
/* GET users listing. */
router.get('/', securityController.tokenVerify,userController.getUsers);
router.delete('/', securityController.tokenVerify,userController.delete);
router.patch('/', securityController.tokenVerify,userController.update);

/* GET users listing. */
router.post('/login', userController.postLogin);
router.post('/profile', securityController.tokenVerify,userController.postProfile);

router.post('/verify',  userController.verifyOTP);

router.post('/refer', userController.postReferral);

/* register a user. */
router.post('/register', userController.postRegister);

module.exports = router;
