const express = require('express');
const router = express.Router();
const multer = require('multer');

/**
 * Importing Controller
 */
const fileController = require('../controllers/file');
const securityController = require('../controllers/security');
router.post('/', securityController.tokenVerify, fileController.addFile);


module.exports = router;