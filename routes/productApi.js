const express = require('express');
const router = express.Router();
const multer = require('multer');



/**
 * Importing Controller
 */
const productsController = require('../controllers/products');
const securityController = require('../controllers/security');

router.get('/', securityController.tokenVerify, productsController.getProducts);
router.get('/temp', securityController.tokenVerify, productsController.getTempProducts);
router.put('/', securityController.tokenVerify, productsController.addTempProduct);
router.patch('/', securityController.tokenVerify, productsController.updateProduct);
router.delete('/', securityController.tokenVerify, productsController.deleteProduct);


module.exports = router;