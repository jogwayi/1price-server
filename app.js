/**
 * Module dependencies.
 */
const express = require('express');
const compression = require('compression');
const bodyParser = require('body-parser');
const logger = require('morgan');
const chalk = require('chalk');
const errorHandler = require('errorhandler');
const dotenv = require('dotenv');
const path = require('path');
const jwt = require('express-jwt');
const unless = require('express-unless');
const helmet = require('helmet');
const expressSanitizer = require('express-sanitizer');
const cloudinary = require('cloudinary');

/**
 * Defining globals
 */
global.uploadsPath = path.resolve("./public/uploads");
global.client = require('./config');
global.mongoose = require('mongoose');

/**
 * Load environment variables from .env file, where API keys and passwords are configured.
 */
dotenv.load({ path: '.env.example' });


/**
 * Cloudinary Config
 * /
cloudinary.config({
    cloud_name: 'dbuh7wgwa',
    api_key: '323642468153157',
    api_secret: 'xKRw_D31_i4NDfM-Aa_5MOEKUUs'
});*/

/**
 * Route Files
 */
const userRoutes = require('./routes/user');
const fileRoutes = require('./routes/fileApi');
const productRoutes = require('./routes/productApi');

/**
 * Create Express server.
 */
const app = express();


/**
 * Token Settings
 */

const jwtCheck = jwt({
    secret: 'KsRMM3dMGZUBzdjBap7UnVnH5ykgjZtK',
    audience: 'http://localhost:3000/auth',
    issuer: "https://1price.au.auth0.com/"
});


/**
 * Connect to MongoDB.
 */
global.mongoose.Promise = global.Promise;
global.mongoose.connect(process.env.MONGODB_URI || process.env.MONGOLAB_URI);
global.mongoose.connection.on('error', (err) => {
    console.error(err);
    console.log('%s MongoDB connection error. Please make sure MongoDB is running.', chalk.red('✗'));
    process.exit();
});


/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3000);
app.use(helmet());
app.use(compression());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressSanitizer());
app.use(express.static(path.join(__dirname, 'public')));
app.use(function(req, res, next) {
    // res.header("Access-Control-Allow-Origin", "*");
    // res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Authorization, Access-Control-Request-Method, Access-Control-Request-Headers");
    next();
});


/**
 * Routing Middleware
 */
app.use('/api/users', userRoutes);
app.use('/api/file', fileRoutes);
app.use('/api/product', productRoutes);

app.use('*', (req, res) => {
    res.status(404).send('NOT FOUND');
});


/**
 * Error Handler.
 */
app.use(errorHandler());

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
    console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env')); 
    console.log('  Press CTRL-C to stop\n');
});

module.exports = app;